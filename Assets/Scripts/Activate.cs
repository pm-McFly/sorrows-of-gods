﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activate : MonoBehaviour
{
    public bool Active = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnTriggerStay(Collider collider)
    {
        if (Input.GetButtonDown("Action") == true && Active == false)
        {
            Active = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}
