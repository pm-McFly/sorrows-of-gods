﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public string element = "";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnTriggerStay(Collider collider)
    {
        Renderer[] statue = GetComponentsInChildren<Renderer>();
        ParticleSystem[] particle = GetComponentsInChildren<ParticleSystem>();
        var weight = collider.GetComponent<Weight>();

        if (Input.GetButtonDown("Action") == true &&  weight.element == "")
        {
            foreach(ParticleSystem e in particle)
                e.GetComponent<ParticleSystem>().Stop();
            weight.SetElement(this.tag);
            GetComponent<Collectible>().enabled = false;
            GetComponent<Collider>().enabled = false;
            foreach(Renderer e in statue)
                e.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
