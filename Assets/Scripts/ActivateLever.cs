﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateLever : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnTriggerStay(Collider collider)
    {
        if (Input.GetButtonDown("Action"))
        {
            var rotationVector = transform.rotation.eulerAngles;
            if (rotationVector.z <= 30) {
                rotationVector.z = rotationVector.z + 90;
            } else {
                rotationVector.z = rotationVector.z - 90;
            }
            transform.rotation = Quaternion.Euler(rotationVector);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}
