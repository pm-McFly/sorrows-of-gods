﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectVisibleCollection : MonoBehaviour
{
    public string tagged = "";

    // Start is called before the first frame update
    void Start()
    {
            GetComponent<Renderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        bool clear = true;
        GameObject[] Statue;

        Statue = GameObject.FindGameObjectsWithTag(tagged);
        foreach(GameObject collectible in Statue) {
            if (collectible.GetComponent<Renderer>().enabled && collectible != this)
                clear = false;
        }
        if (clear)
            GetComponent<Renderer>().enabled = true;
    }
}
