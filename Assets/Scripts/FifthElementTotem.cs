﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FifthElementTotem : MonoBehaviour
{
    private string water = "Water";
    private string wind = "Wind";
    private string fire = "Fire";
    private string earth = "Earth";
    private bool state = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject braserosFire;
        GameObject braserosWater;
        GameObject braserosEarth;
        GameObject braserosWind;

        braserosEarth = GameObject.FindWithTag(earth);
        braserosFire = GameObject.FindWithTag(fire);
        braserosWater = GameObject.FindWithTag(water);
        braserosWind = GameObject.FindWithTag(wind);

        if (braserosEarth.GetComponent<ParticleSystem>().isPlaying == true
            && braserosFire.GetComponent<ParticleSystem>().isPlaying == true
            && braserosWater.GetComponent<ParticleSystem>().isPlaying == true
            && braserosWind.GetComponent<ParticleSystem>().isPlaying == true
            && state == true)
        {
            state = false;
            if (GetComponent<ParticleSystem>())
                GetComponent<ParticleSystem>().Play();
            if (GetComponent<Renderer>())
                GetComponent<Renderer>().enabled = true;
            if(GetComponent<Collectible>())
                GetComponent<Collectible>().enabled = true;
        }

    }
}
