﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockTriggered : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnTriggerEnter(Collider collider)
    {
        GameObject Rock;

        Rock = GameObject.FindWithTag("Rock");
        Rock.GetComponent<Rigidbody>().useGravity = true;
        GetComponent<AudioSource>().Play(0);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
