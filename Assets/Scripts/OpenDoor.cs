﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    public string element = "";
    public string color = "";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnTriggerStay(Collider collider) {
        GameObject[] TaggedElement;
        GameObject TaggedTotem;
        Renderer[] totemRenderer;
        ParticleSystem[] totemParticles;

        TaggedTotem = GameObject.FindWithTag(color);
        TaggedElement = GameObject.FindGameObjectsWithTag(element);
        totemRenderer = TaggedTotem.GetComponentsInChildren<Renderer>();
        totemParticles = TaggedTotem.GetComponentsInChildren<ParticleSystem>();
        var weight = collider.GetComponent<Weight>();
        if (Input.GetButtonDown("Action") == true && collider.GetComponent<Weight>().GetElement() == color)
        {
            foreach(Renderer e in totemRenderer)
                e.enabled = true;
            foreach(ParticleSystem e in totemParticles)
                e.GetComponent<ParticleSystem>().Play();
            weight.SetElement("");
            TaggedTotem.GetComponent<Light>().enabled = true;
            TaggedTotem.GetComponent<Transform>().position = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z);
            foreach(GameObject objectGame in TaggedElement) {
                if (objectGame.GetComponent<MeshCollider>()) {
                    objectGame.GetComponent<MeshCollider>().enabled = false;
                    objectGame.GetComponent<Renderer>().enabled = false;
                }
                if (objectGame.GetComponent<ParticleSystem>()) {
                    objectGame.GetComponent<Renderer>().enabled = true;
                    objectGame.GetComponent<ParticleSystem>().Play();
                }
                if (objectGame.GetComponent<Light>())
                    objectGame.GetComponent<Light>().enabled = true;
            }
        }

    }
    // Update is called once per frame
    void Update()
    {
    }
}
